import React, { Component } from "react";
import { StyleProvider, Root } from "native-base";
import { AppLoading } from "expo";
import { I18nextProvider } from "react-i18next";
import moment from "moment/min/moment-with-locales";
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import Sentry from 'sentry-expo';

import AppNavigator from "./src/AppNavigator";
import getTheme from "./native-base-theme/components";
import variables from "./native-base-theme/variables/commonColor";
import { getConfigKey } from "./src/helpers";

import i18n from "./src/i18n";
import { store, persistor } from './src/redux/createStore';

Sentry.config(getConfigKey("SentryDSN")).install();

export default class App extends Component {
  render() {
    moment.locale(i18n.language);
    return <I18nextProvider i18n={i18n}>
      <StyleProvider style={getTheme(variables)}>
        <Root>
          <Provider store={store}>
            <PersistGate persistor={persistor} loading={<AppLoading />}>
              <AppNavigator />
            </PersistGate>
          </Provider>
        </Root>
      </StyleProvider>
    </I18nextProvider>
  }
}
