import React, { Component } from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { translate } from "react-i18next";
import { connect } from "react-redux";
import { Dimensions, TouchableOpacity } from "react-native";
import { EvilIcons } from "@expo/vector-icons";

import Login from "./containers/login";
import Settings from "./containers/settings";
import Home from "./containers/home";
import NewBooking from "./containers/newbooking";
import Drawer from "./containers/drawer";
import ListBookings from "./containers/listbookings";

import variables from "../native-base-theme/variables/commonColor";

const MainNavigator = DrawerNavigator({
  Home: { screen: Home },
  NewBooking: { screen: NewBooking },
  ListBookings: {screen: ListBookings},
  Settings: { screen: Settings },
}, {
    drawerWidth: Dimensions.get("window").width,
    contentComponent: Drawer
  });

const stackNavigatorScreens = {
  Login: { screen: Login },
  Main: { screen: MainNavigator },
}

class AppNavigator extends Component {
  render() {
    const { t, userId } = this.props;
    const initialRouteName = userId !== null ? "Main" : "Login";
    const stackNavigatorConfigs = {
      initialRouteName,
      navigationOptions: ({ navigation }) => ({
        headerLeft: <TouchableOpacity style={{ padding: 5 }} onPress={() => navigation.navigate("DrawerToggle")} >
                <EvilIcons name="navicon" size={32} color={variables.gray} />
              </TouchableOpacity>
      })
    };
    const CustomNavigator = StackNavigator(stackNavigatorScreens, stackNavigatorConfigs);
    return <CustomNavigator screenProps={{ t }} />
  }
}

const mapStateToProps = state => ({
  userId: state.profile.userId,
});

export default translate()(connect(mapStateToProps)(AppNavigator));
