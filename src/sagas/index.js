import { all, takeLatest } from 'redux-saga/effects';
import { REHYDRATE } from "redux-persist";

import { AuthTypes } from "../redux/AuthRedux";
import { ProfileTypes } from "../redux/ProfileRedux";
import { BookingTypes } from "../redux/BookingRedux";
import { FacilityTypes } from "../redux/FacilityRedux";

import { login, renewToken } from "./AuthSagas";
import { getProfile } from './ProfileSagas';
import { getBookingList, addBooking, deleteBooking } from "./BookingSagas";
import { startup } from "./StartupSagas";
import { getFacilityList } from "./FacilitySagas";

export default function* root() {
  yield all([
    takeLatest(REHYDRATE, startup),
    takeLatest(AuthTypes.LOGIN_REQUEST, login),
    takeLatest(AuthTypes.LOGIN_RESPONSE, renewToken),
    takeLatest(AuthTypes.UPDATE_TOKEN, renewToken),
    takeLatest(ProfileTypes.PROFILE_REQUEST, getProfile),
    takeLatest(BookingTypes.ADD_BOOKING_REQUEST, addBooking),
    takeLatest(BookingTypes.GET_BOOKING_LIST_REQUEST, getBookingList),
    takeLatest(BookingTypes.DELETE_BOOKING_REQUEST, deleteBooking),
    takeLatest(FacilityTypes.GET_FACILITY_LIST_REQUEST, getFacilityList),
  ]);
}
