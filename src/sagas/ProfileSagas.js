import { call, put } from "redux-saga/effects";
import Sentry from "sentry-expo";

import Api from "../services/Api";
import ProfileActions from "../redux/ProfileRedux";

export function* getProfile({ consortiumCode, functionalUnitCode, userId }) {
  try {
    const url = `consortium/${consortiumCode}/functionalunit/${functionalUnitCode}/user/${userId}`;
    const response = yield call([Api, 'get'], url);
    if (response.ok) {
      yield put(ProfileActions.profileResponse(response.data));
    } else {
      const msg = `Error in ProfileSagas - getProfile userId ${userId} - response: ${JSON.stringify(response)}`;
      Sentry.captureException(msg);
      yield put(ProfileActions.profileError(response.problem));
    }
  } catch (err) {
    const msg = `Error in ProfileSagas - getProfile userId ${userId} - error: ${err}`;
    Sentry.captureException(msg);
    yield put(ProfileActions.profileError(err));
  }
}
