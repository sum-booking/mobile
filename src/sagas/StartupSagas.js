import { select, call, fork } from "redux-saga/effects";
import { getAccessToken } from "./Selectors";
import { setApiAuthorization } from "./AuthSagas";
import { renewToken } from "./AuthSagas";

export function* startup() {
  const accessToken = yield select(getAccessToken);
  if (accessToken) {
    yield fork(renewToken)
    yield call(setApiAuthorization, { accessToken });
  }
}
