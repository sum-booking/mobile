import { select, put, call } from "redux-saga/effects";
import Sentry from 'sentry-expo';

import BookingActions from "../redux/BookingRedux";
import Api from "../services/Api";

import { getConsortiumCode, getFunctionalUnitCode, getUserEmail, getBookings } from "./Selectors";

export function* getBookingList({ from, to, userId }) {
  try {
    const consortiumCode = yield select(getConsortiumCode);
    const response = yield call([Api, 'get'], `consortium/${consortiumCode}/booking`, { from, to, userId });
    if (response.ok) {
      yield put(BookingActions.getBookingListResponse(response.data));
    } else if (response.status === 404) {
      yield put(BookingActions.getBookingListResponse([]));
    } else {
      Sentry.captureException(response.problem);
      yield put(BookingActions.getBookingListFailed(response.problem))
    }
  } catch(err) {
    Sentry.captureException(err);
    yield put(BookingActions.getBookingListFailed(err))
  }
}

export function* addBooking({ form }) {
  try {
    const consortiumCode = yield select(getConsortiumCode);
    const functionalUnitCode = yield select(getFunctionalUnitCode);
    const createdBy = yield select(getUserEmail);
    const createdOn = new Date();
    const request = {
      functionalUnitCode,
      createdBy,
      createdOn
    };
    for (key in form) {
      request[key] = form[key].value;
    }
    const response = yield call([Api, 'post'], `consortium/${consortiumCode}/booking`, request);
    if (response.ok) {
      yield put(BookingActions.addBookingResponse());
    } else {
      if (response.data) {
        const errorKeys = Object.keys(response.data);
        if (errorKeys && errorKeys.length > 0) {
          const errorCode = errorKeys[0];
          yield put(BookingActions.addBookingFailed(errorCode));
        } else {
          Sentry.captureException(response);
          yield put(BookingActions.addBookingFailed("UNKNOWN_ERROR"));
        }
      } else {
        Sentry.captureException(response);
        yield put(BookingActions.addBookingFailed("UNKNOWN_ERROR"));
      }
    }
  } catch(err) {
    Sentry.captureException(err);
    yield put(BookingActions.addBookingFailed("UNKNOWN_ERROR"))
  }
}

export function* deleteBooking({ id }) {
  try {
    const consortiumCode = yield select(getConsortiumCode);
    const updatedBy = yield select(getUserEmail);
    const updatedOn = new Date();
    const request = {
      updatedBy,
      updatedOn
    };
    const response = yield call([Api, 'delete'], `consortium/${consortiumCode}/booking/${id}`, request);
    if (response.ok) {
      let bookings = yield select(getBookings);
      bookings = bookings.filter(x => x.id !== id);
      yield put(BookingActions.getBookingListResponse(bookings));
      yield put(BookingActions.deleteBookingResponse());
    } else {
      if (response.data) {
        const errorKeys = Object.keys(response.data);
        if (errorKeys && errorKeys.length > 0) {
          const errorCode = errorKeys[0];
          yield put(BookingActions.deleteBookingFailed(errorCode));
        } else {
          Sentry.captureException(response);
          yield put(BookingActions.deleteBookingFailed("UNKNOWN_ERROR"));
        }
      } else {
        Sentry.captureException(response);
        yield put(BookingActions.deleteBookingFailed("UNKNOWN_ERROR"));
      }
    }
  } catch(err) {
    Sentry.captureException(err);
    yield put(BookingActions.deleteBookingFailed("UNKNOWN_ERROR"))
  }
}
