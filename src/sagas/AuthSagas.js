import { put, call, select } from "redux-saga/effects";
import { delay } from "redux-saga";
import { create } from 'apisauce'
import { Util } from "expo";
import jwtDecode from "jwt-decode";
import Sentry from 'sentry-expo';

import AuthActions from "../redux/AuthRedux";
import ProfileActions from "../redux/ProfileRedux";

import { persistor } from "../redux/createStore";
import { getRefreshToken, getExpiresAt } from "./Selectors";
import Api from "../services/Api";

import { getConfigKey } from "../helpers";

const TIMEOUT_THRESHOLD = 5 * 1000 * 60;

export function* login({ request }) {
  // see https://auth0.com/docs/api-auth/tutorials/authorization-code-grant-pkce
  try {
    if (request.error) {
      yield put(AuthActions.loginError(request.error));
    } else {
      const auth0ClientId = getConfigKey("auth0ClientId");
      const auth0Domain = getConfigKey("auth0Domain");
      const authRequest = {
        grant_type: "authorization_code",
        client_id: auth0ClientId,
        code: request.code,
        redirect_uri: request.redirect_uri,
        state: request.state
      };
      const api = create({
        baseURL: `https://${auth0Domain}`,
        headers: { 'Accept': 'application/json' },
      });
      const response = yield call([api, 'post'], 'oauth/token', authRequest);
      if (response.data && response.data.id_token) {
        const data = response.data;
        const decodedToken = jwtDecode(data.id_token);
        const expiresAt = (data.expires_in * 1000) + new Date().getTime();
        yield call(setApiAuthorization, { accessToken: data.access_token });
        yield put(ProfileActions.profileRequest(
          decodedToken["https://sumbooking.com/consortiumCode"],
          decodedToken["https://sumbooking.com/functionalUnitCode"],
          decodedToken.sub,
        ));
        yield put(AuthActions.loginResponse(data.access_token, data.refresh_token, expiresAt));
      } else {
        const msg = `Error in AuthSagas - login, auth0 response: ${JSON.stringify(response)}`;
        Sentry.captureException(msg);
        yield put(AuthActions.loginError(msg));
      }
    }
  } catch (err) {
    const msg = `Error in AuthSagas - login, parameters: ${JSON.stringify(request)}, error: ${err}`;
    Sentry.captureException(msg);
    yield put(AuthActions.loginError(msg));
  }
}

export function* renewToken() {
  try {
    const expiresAt = yield select(getExpiresAt);
    const timeout = expiresAt - TIMEOUT_THRESHOLD - new Date().getTime();
    yield delay(timeout);
    const refreshToken = yield select(getRefreshToken);
    if (refreshToken) {
      const auth0ClientId = getConfigKey("auth0ClientId");
      const auth0Domain = getConfigKey("auth0Domain");
      const api = create({
        baseURL: `https://${auth0Domain}`,
        headers: { 'Accept': 'application/json' },
      });
      const request = {
        grant_type: "refresh_token",
        refresh_token: refreshToken,
        client_id: auth0ClientId
      };
      const response = yield call([api, 'post'], 'oauth/token', request);
      if (response.ok) {
        const data = response.data;
        const newExpiresAt = (data.expires_in * 1000) + new Date().getTime();
        yield put(AuthActions.updateToken(data.access_token, newExpiresAt));
        yield call(setApiAuthorization, { accessToken: data.access_token });
      } else {
        yield call([persistor, 'purge']);
        Util.reload();
      }
    }
  } catch (err) {
    Sentry.captureException(err);
    yield call([persistor, 'purge']);
    Util.reload();
  }
}

export function* setApiAuthorization({ accessToken }) {
  yield call([Api, 'setHeader'], 'Authorization', `Bearer ${accessToken}`);
}
