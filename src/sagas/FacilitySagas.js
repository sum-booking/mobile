import { select, put, call } from "redux-saga/effects";
import Sentry from 'sentry-expo';

import FacilityActions from "../redux/FacilityRedux";
import Api from "../services/Api";

import { getConsortiumCode } from "./Selectors";

export function* getFacilityList() {
  try {
    const consortiumCode = yield select(getConsortiumCode);
    const response = yield call([Api, 'get'], `consortium/${consortiumCode}/facility`);
    if (response.ok) {
      yield put(FacilityActions.getFacilityListResponse(response.data));
    } else if (response.status === 404) {
      yield put(FacilityActions.getFacilityListResponse([]));
    } else {
      yield put(FacilityActions.getFacilityListFailed(response.problem));
    }
  } catch (err) {
    Sentry.captureException(err);
    yield put(FacilityActions.getFacilityListFailed(err));
  }
}
