export const getRefreshToken = state => state.auth.refreshToken;
export const getConsortiumCode = state => state.profile.consortiumCode
export const getAccessToken = state => state.auth.accessToken;
export const getExpiresAt = state => state.auth.expiresAt;
export const getFunctionalUnitCode = state => state.profile.functionalUnitCode;
export const getUserEmail = state => state.profile.email;
export const getBookings = state => state.booking.bookings;
