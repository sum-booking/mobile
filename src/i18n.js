// @flow
import i18n from "i18next";
import Expo from "expo";

const languageDetector = {
    type: "languageDetector",
    async: true, // async detection
    detect: (cb) => {
        return Expo.Util.getCurrentLocaleAsync()
            .then(lng => {
                if (lng.indexOf("-") !== -1)
                    cb(lng.split("-")[0]);
                else if (lng.indexOf("_") !== -1)
                    cb(lng.split("_")[0]);
                else
                    cb(lng);
            });
    },
    init: () => { },
    cacheUserLanguage: () => { }
}

i18n
    .use(languageDetector)
    .init({
        fallbackLng: "es",

        // the translations
        resources: {
            es: require("./locales/es.json")
        },

        // have a initial namespace
        ns: ["translation"],
        defaultNS: "translation",
        interpolation: {
            escapeValue: false // not needed for react
        }
    });

export default i18n;