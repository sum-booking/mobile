// @flow
import * as _ from "lodash";
import React, { Component } from "react";
import { ListItem, Item, Label, Input, Body, Right, Icon } from "native-base";

import variables from "../../native-base-theme/variables/commonColor";

interface FieldProps {
    label: string,
    defaultValue?: string,
    last?: boolean,
    inverse?: boolean,
    right?: () => React$Element<*>
}

export default class Field extends Component {

    props: FieldProps;

    render(): React$Element<*> {
        const { label, last, disabled, defaultValue, right, error,
            readonly } = this.props;
        const style = disabled ? { color: "#ccc" } : {};
        const labelStyle = disabled ? { color: "#000" } : error ? { color: variables.brandDanger } : {};
        const keysToFilter = ["right", "defaultValue", "inverse", "label", "last"];
        const props = _.pickBy(this.props, (value, key) => keysToFilter.indexOf(key) === -1);
        const { value } = this;
        const errorStyle = error ? { borderBottomColor: variables.brandDanger } : undefined
        const shouldDisable = disabled || readonly
        return <ListItem {...{ last }} style={errorStyle}>
            <Body>
                <Item
                    error={error}
                    style={{ borderBottomWidth: 0 }}
                    floatingLabel={!defaultValue}
                    stackedLabel={!!defaultValue}>
                    <Label style={labelStyle} >{label}</Label>
                    <Input  {...{ value, style }} {...props}
                        disabled={shouldDisable} />
                </Item>
            </Body>
            {
                right && <Right>{right()}</Right>
            }
            {
                error && <Right>
                    <Icon name='close-circle' style={{ color: variables.brandDanger }} />
                </Right>
            }
        </ListItem>;
    }
}