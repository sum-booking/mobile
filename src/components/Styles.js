import {StyleSheet, Dimensions} from "react-native";

import variables from "../../native-base-theme/variables/commonColor";

const {width} = Dimensions.get("window");
const Styles = StyleSheet.create({
    imgMask: {
        backgroundColor: "rgba(58, 132, 42, .9)",
    },
    header: {
        width,
        height: width * 440 / 750
    },
    flexGrow: {
        flex: 1
    },
    center: {
        justifyContent: "center",
        alignItems: "center"
    },
    textCentered: {
        textAlign: "center"
    },
    bg: {
        backgroundColor: "white"
    },
    row: {
        flexDirection: "row"
    },
    whiteBg: {
        backgroundColor: "white"
    },
    whiteText: {
        color: "white"
    },
    grayText: {
        color: variables.gray
    },
    listItem: {
        flexDirection: "row",
        borderBottomWidth: variables.borderWidth,
        borderColor: variables.listBorderColor
    },
    emptyDate: {
      flex: 1,
      alignItems: "center",
      justifyContent: "space-around",
  },
  dayNum: {
      fontSize: 28,
      fontWeight: "200",
      color: "#7a92a5"
  },
  dayText: {
      fontSize: 14,
      fontWeight: "300",
      color: "#7a92a5",
      backgroundColor: "rgba(0,0,0,0)"
  },
  today: {
      color: "#00adf5"
  },
  topBorder: {
      borderTopColor: variables.listBorderColor,
      borderTopWidth: 1,
  },
  bookStrategy: {
      flex: 0.2,
      alignItems: "center",
      justifyContent: "space-around",
      borderRightColor: variables.listBorderColor,
      borderRightWidth: 1,
      paddingTop: variables.contentPadding,
      paddingBottom: variables.contentPadding,
  },
  bookText: {
    color: variables.brandSecondary
  },
  gray: {
      color: variables.gray
  },
  description: {
      justifyContent: "center",
      flex: 0.8,
      padding: variables.contentPadding
  },
  item: {
      flexDirection: "row",
      alignItems: "stretch",
      justifyContent: "space-around",
      height: 60,
  },
  day: {
      width: 45,
      alignItems: "center",
      justifyContent: "center",
  },
});

export default Styles;
