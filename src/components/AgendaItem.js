import React, { Component } from "react";
import { View, Text } from "react-native";
import { translate } from "react-i18next";

import styles from "./Styles";

export default class AgendaItem extends Component {
    render() {
        const { facility, by, firstItemInDay, isMidDay, isNight, strategy } = this.props;
        const topBorder = firstItemInDay ? styles.topBorder : undefined;
        return <View style={[styles.item, topBorder]}>
            <StrategySchedule isMidDay={isMidDay} isNight={isNight} strategy={strategy} />
            <DescriptionColumn facility={facility} by={by} />
        </View>;
    }
}

const StrategySchedule = ({ strategy, isMidDay, isNight }) => (
    strategy === "MidDayAndNight"
      ? <TranslatedMidDayAndNightStrategySchedule isMidDay={isMidDay} isNight={isNight} />
      : <TranslatedNightOnlyStrategySchedule />
);

const NightOnlyStrategySchedule = ({t}) => (
  <View style={styles.bookStrategy}>
        <Text style={styles.bookText}>{t("booking:night")}</Text>
    </View>
)
const TranslatedNightOnlyStrategySchedule = translate()(NightOnlyStrategySchedule);

const MidDayAndNightStrategySchedule = ({t, isNight }) => (
  <View style={styles.bookStrategy}>
        <Text style={styles.bookText}>{isNight ? t("booking:night") : t("booking:midDay")}</Text>
    </View>
)
const TranslatedMidDayAndNightStrategySchedule = translate()(MidDayAndNightStrategySchedule);

const DescriptionColumn = ({ facility, by }) => (
    <View style={styles.description}>
        <Text>{facility}</Text>
        <View style={styles.row}>
            <Text style={styles.gray}>{by}</Text>
        </View>
    </View>
);
