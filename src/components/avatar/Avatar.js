// @flow
import React, { Component } from "react";
import { Image, StyleSheet } from "react-native";
import { Spinner } from "native-base";

export default class Avatar extends Component {
    state = {
        prefetch: false,
        error: false,
    }

    props: {
        size: number,
        picture: string,
        style?: StyleSheet.Styles | Array<StyleSheet.Styles>
    }

    static defaultProps = {
        size: 20,
        id: 0,
        picture: null,
    }

    componentDidMount() {
        if (this.props.picture) {
            Image.prefetch(this.props.picture).then(() => {
                this.setState({ prefetched: true });
            }).catch(() => {
                this.setState({ prefetched: true, error: true });
            })
        } else {
            this.setState({ prefetched: true });
        }
    }

    render(): React$Element<*> {
        const { size, picture, style } = this.props;
        const { prefetched, error } = this.state;
        let source = picture && !error
            ? { uri: picture }
            : require("../../../assets/images/default-avatar.png")

        return prefetched
            ? <Image {...{ source }}
                style={[style, {
                    width: size, height: size, borderRadius: size / 2,
                    borderColor: "grey", borderWidth: 1
                }]} />
            : <Spinner
                style={{
                    height: size, width: size, borderColor: "grey", borderWidth: 1,
                    borderRadius: size / 2
                }} color="grey" />
    }
}