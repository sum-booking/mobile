import { create } from 'apisauce'
import { getConfigKey } from "../helpers";

// define the api
export default create({
  baseURL: getConfigKey("ApiUri"),
  headers: { 'Accept': 'application/json' },
});
