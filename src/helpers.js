import Expo from "expo";

export const capitalizeFirstLetter = string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export const capitalizeFirstLetterArray = arr => {
    const ret = [arr.length]
    arr.forEach((item, i) => {
        ret[i] = capitalizeFirstLetter(item);
    })
    return ret;
}

export const getConfigKey = key => {
    const extra = Expo.Constants.manifest.extra;
    const channel =  __DEV__ ? "DEV" : Expo.Constants.manifest.releaseChannel;
    const setting = __DEV__ ? extra[channel][key] : extra[channel][key]
    return setting ? setting : extra.DEV[key];
}

export const toQueryString = params => "?" + Object.entries(params)
    .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
    .join("&");

export const guid = (separator = "-") => {
    const s4 = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + separator + s4() + separator + s4() + separator +
        s4() + separator + s4() + s4() + s4();
}

export const hashString = (str) => {
    var hash = 0, i, chr;
    if (str.length === 0) return hash;
    for (i = 0; i < str.length; i++) {
        chr = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
