import React, { Component } from "react";
import { ScrollView, Button } from "react-native";
import { Container, List, Spinner, Toast } from "native-base";
import { connect } from "react-redux";
import { translate } from "react-i18next";

import FormActions from "../../redux/FormRedux";
import BookingActions from "../../redux/BookingRedux";

import FacilitiesSelect from "./components/FacilitiesSelect";
import DateTimePickerInput from "./components/DateTimePickerInput";
import BookingSchedule from "./components/BookingSchedule";

class Booking extends Component {
  static navigationOptions = ({ screenProps, navigation }) => {
    const { t } = screenProps;
    const { params = {} } = navigation.state;
    let headerRight = <Button title={t("common:save")}
      onPress={params.handleSave ? params.handleSave : () => null} />
    let headerLeft = undefined;
    if (params.isSaving) {
      headerRight = <Spinner size="small" color="grey" style={{ paddingRight: 10 }} />;
      headerLeft = null;
    }
    return {
      headerTitle: t("booking:addTitle"),
      headerRight: headerRight,
      headerLeft: headerLeft,
    }
  }

  constructor(props) {
    super(props);

    props.initForm('booking');
    props.navigation.setParams({ handleSave: this.handleSave });

    this.state = {
      bookDate: props.navigation.state.params.date,
      bookingSchedule: null,
    }
  }

  componentDidUpdate(prevProps) {
    const { adding, navigation, saveError, t } = this.props;
    if (prevProps.adding !== adding) {
      navigation.setParams({ isSaving: adding });

      if (!adding) {
        navigation.setParams({ isSaving: false });
        if (!saveError) {
          navigation.state.params.onNavigateBack();
          navigation.goBack();
        } else {
          Toast.show({
            text: t(`api:${saveError}`),
            type: "warning",
            buttonText: t("common:ok"),
            duration: 5000,
            position: "top"
          });
        }
      }
    }
  }

  handleSave = () => {
    let isValid = true;
    for (key in this.props.form) {
      const field = this.props.form[key];
      isValid = isValid && field.valid;
    }
    if (isValid) {
      this.props.saveBooking(this.props.form);
    }
  }

  changeFacility = (option) => {
    this.setState({ bookingSchedule: option.bookingSchedule });
  }

  render() {
    return <Container>
      <ScrollView style={{ backgroundColor: "white" }}>
        <List>
          <FacilitiesSelect facilityChange={this.changeFacility} />
          <DateTimePickerInput currentDateTime={this.state.bookDate} mode="date" />
          <BookingSchedule bookingSchedule={this.state.bookingSchedule} />
        </List>
      </ScrollView>
    </Container>;
  }
}

const mapStateToProps = state => ({
  form: state.form.forms.booking,
  adding: state.booking.isAdding,
  saveError: state.booking.addFailedMessage,
});

const mapDispatchToProps = dispatch => ({
  initForm: (form) => dispatch(FormActions.initForm(form)),
  saveBooking: (form) => dispatch(BookingActions.addBookingRequest(form)),
});

export default translate()(connect(mapStateToProps, mapDispatchToProps)(Booking));
