import React, { Component } from "react";
import { ListItem, Body, Right, Icon, Item, Label, Input, Spinner } from "native-base";
import { translate } from "react-i18next";
import { TouchableWithoutFeedback } from "react-native";
import { connect } from "react-redux";

import FacilityActions from "../../../redux/FacilityRedux";
import FormActions from "../../../redux/FormRedux";

import ModalSelector from "../../../components/modal-selector";
import variables from "../../../../native-base-theme/variables/commonColor";

class FacilitiesSelect extends Component {
  state = {
    facilitySelectedId: "",
    facilitySelectedName: "",
    showSelect: false
  }

  componentDidMount() {
    const { getFacilities, updateField } = this.props;
    updateField(null, false);
    getFacilities();
  }

  handleOptionChange = (option: object) => {
    this.setState({
      facilitySelectedId: option.key,
      facilitySelectedName: option.label,
      showSelect: false
    });
    const facility = this.props.facilities.find(x => x.id === option.key);
    this.props.updateField(option.key, true);
    this.props.facilityChange({ ...option, bookingSchedule: facility.bookingSchedule });
  }

  handleShowSelect = () => {
    this.setState({ showSelect: true })
  }

  render() {
    const { t, valid, fetching, facilities } = this.props;
    const listErrorStyle = !valid ? { borderBottomColor: variables.brandDanger } : undefined
    const colorErrorStyle = !valid ? { color: variables.brandDanger } : undefined;
    const options = facilities.map(item => ({ key: item.id, label: item.name }));
    return <TouchableWithoutFeedback onPress={this.handleShowSelect}
      disabled={fetching}>
      <ListItem style={[{ padding: 0, margin: 0 }, listErrorStyle]}>
        <Body style={{ padding: 0, margin: 0 }}>
          <ModalSelector
            open={this.state.showSelect}
            optionContainerStyle={{ borderRadius: 15, }}
            optionStyle={{ padding: 15 }}
            cancelStyle={{ padding: 15, borderRadius: 15 }}
            cancelContainerStyle={{ marginTop: 20 }}
            cancelText={t("common:cancel")}
            data={options}
            onChange={this.handleOptionChange}
            onCancel={() => this.setState({ showSelect: false })}>
            <Item
              style={{ borderBottomWidth: 0 }}
              floatingLabel={!this.state.facilitySelectedName}
              stackedLabel={!!this.state.facilitySelectedName}
            >
              <Label style={colorErrorStyle}>{t("booking:facilityToBook")}</Label>
              <Input value={this.state.facilitySelectedName} />
            </Item>
          </ModalSelector>
        </Body>
        <Right>
          {fetching
            ? <Spinner size="small" color="black" />
            : <Icon name="more" style={colorErrorStyle} />
          }
        </Right>
      </ListItem>
    </TouchableWithoutFeedback>

  }
}

const mapStateToProps = state => ({
  fetching: state.facility.isFetchingFacilities,
  facilities: state.facility.facilities,
  valid: (!state.form.forms['booking'] || !state.form.forms['booking'].facilityId)
    ? false
    : state.form.forms['booking'].facilityId.valid,
});

const mapDispatchToProps = dispatch => ({
  getFacilities: () => dispatch(FacilityActions.getFacilityListRequest()),
  updateField: (value, valid) => dispatch(FormActions.updateField('booking', 'facilityId', value, valid)),
})

export default translate()(connect(mapStateToProps, mapDispatchToProps)(FacilitiesSelect));
