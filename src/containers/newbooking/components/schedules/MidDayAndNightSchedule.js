import React, { Component } from "react";
import { ListItem, Body, Right, Icon, Item, Label, Input } from "native-base";
import { translate } from "react-i18next";
import { TouchableWithoutFeedback } from "react-native";
import { connect } from "react-redux";

import FormActions from "../../../../redux/FormRedux";

import ModalSelector from "../../../../components/modal-selector";
import variables from "../../../../../native-base-theme/variables/commonColor";

class MidDayAndNightSchedule extends Component {
  state = {
    bookingTimeId: "",
    bookingTimeName: "",
    showSelect: false
  }

  componentDidMount() {
    this.props.updateIsMidDayField(null, false);
    this.props.updateIsNightField(null, false);
  }

  handleOptionChange = (option: object) => {
    this.setState({
      bookingTimeId: option.key,
      bookingTimeName: option.label,
      showSelect: false
    });
    this.props.updateIsMidDayField(option.key === "midDay", true);
    this.props.updateIsNightField(option.key === "night", true);
  }

  handleShowSelect = () => {
    this.setState({ showSelect: true })
  }

  componentWillUnmount() {
    this.props.removeField('isMidDay');
    this.props.removeField('isNight');
  }

  options = () => {
    const { t } = this.props;
    return [
      { key: "midDay", label: t("booking:midDay") },
      { key: "night", label: t("booking:night") }
    ]
  }

  render() {
    const { t, valid } = this.props;
    const listErrorStyle = !valid ? { borderBottomColor: variables.brandDanger } : undefined
    const colorErrorStyle = !valid ? { color: variables.brandDanger } : undefined;
    return <TouchableWithoutFeedback onPress={this.handleShowSelect} >
      <ListItem style={[{ padding: 0, margin: 0 }, listErrorStyle]}>
        <Body style={{ padding: 0, margin: 0 }}>
          <ModalSelector
            open={this.state.showSelect}
            optionContainerStyle={{ borderRadius: 15, }}
            optionStyle={{ padding: 15 }}
            cancelStyle={{ padding: 15, borderRadius: 15 }}
            cancelContainerStyle={{ marginTop: 20 }}
            cancelText={t("common:cancel")}
            data={this.options()}
            onChange={this.handleOptionChange}
            onCancel={() => this.setState({ showSelect: false })}>
            <Item
              style={{ borderBottomWidth: 0 }}
              floatingLabel={!this.state.bookingTimeName}
              stackedLabel={!!this.state.bookingTimeName}
            >
              <Label style={colorErrorStyle}>{t("booking:timeframe")}</Label>
              <Input value={this.state.bookingTimeName} />
            </Item>
          </ModalSelector>
        </Body>
        <Right>
          <Icon name="more" style={colorErrorStyle} />
        </Right>
      </ListItem>
    </TouchableWithoutFeedback>
  }
}

const mapStateToProps = state => ({
  valid: (!state.form.forms['booking'] || !state.form.forms['booking'].isMidDay)
    ? false
    : state.form.forms['booking'].isMidDay.valid,
});

const mapDispatchToProps = dispatch => ({
  updateIsMidDayField: (value, valid) => dispatch(FormActions.updateField('booking', 'isMidDay', value, valid)),
  updateIsNightField: (value, valid) => dispatch(FormActions.updateField('booking', 'isNight', value, valid)),
  removeField: field => dispatch(FormActions.removeField('booking', field)),
})

export default translate()(connect(mapStateToProps, mapDispatchToProps)(MidDayAndNightSchedule));
