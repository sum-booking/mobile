import React, { Component } from "react";
import { View } from "react-native";

import NightOnlySchedule from "./schedules/NightOnlySchedule";
import MidDayAndNightSchedule from "./schedules/MidDayAndNightSchedule";

let strategies = [
  { type: "NightOnly", component: (props) => <NightOnlySchedule {...props} /> },
  { type: "MidDayAndNight", component: (props) => <MidDayAndNightSchedule {...props} /> }
];

class BookingSchedule extends Component {
  render() {
    var strategy = strategies.find(x => x.type === this.props.bookingSchedule);
    if (strategy) {
      return strategy.component(this.props);
    }
    return <View />
  }
}

export default BookingSchedule;

