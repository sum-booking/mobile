import moment from "moment/min/moment-with-locales";
import React, { Component } from "react";
import { Text } from "react-native";
import { ListItem, Body, Right, Icon, Label } from "native-base";
import { translate } from "react-i18next";
import DateTimePicker from "react-native-modal-datetime-picker";
import { connect } from "react-redux";

import FormActions from "../../../redux/FormRedux";

import { capitalizeFirstLetter } from "../../../helpers";
import variables from "../../../../native-base-theme/variables/commonColor";

class DateTimePickerInput extends Component {
  constructor(props) {
    super(props);

    let currentDateTime = moment().toDate();
    if (props.currentDateTime) {
      currentDateTime = moment(props.currentDateTime).toDate();
    }
    props.updateField(currentDateTime, true);
    this.state = {
      isPickerVisible: false,
      currentDateTime: currentDateTime,
    }
  }

  handleDateTimePicked = (date) => {
    this.setState({ isPickerVisible: false, currentDateTime: date });
    this.props.updateField(date, true);
  }

  render() {
    const { i18n, t, valid, mode } = this.props;
    const { currentDateTime } = this.state
    const currentDateTimeText = capitalizeFirstLetter(moment(currentDateTime).locale(i18n.language).format("dddd, D MMMM YYYY"));
    const listErrorStyle = !valid ? { borderBottomColor: variables.brandDanger } : undefined
    const colorErrorStyle = !valid ? { color: variables.brandDanger } : { color: "#ccc" };
    return <ListItem onPress={() => this.setState({ isPickerVisible: true })} style={listErrorStyle}>
      <Body>
        <Label style={[{
          fontSize: 15, lineHeight: 30
        }, colorErrorStyle]}>{this.props.label}</Label>
        <Text style={{ lineHeight: 30, }}>{currentDateTimeText}</Text>

      </Body>
      <Right>
        <Icon name="calendar" style={colorErrorStyle} />
      </Right>
      <DateTimePicker
        isVisible={this.state.isPickerVisible}
        onConfirm={this.handleDateTimePicked}
        mode={mode}
        cancelTextIOS={t("common:cancel")}
        confirmTextIOS={t("common:confirm")}
        date={currentDateTime}
        titleIOS={t("booking:pickDate")}
        onCancel={() => this.setState({ isPickerVisible: false })}
      />
    </ListItem>
  }
}

const mapStateToProps = state => ({
  valid: (!state.form.forms['booking'] || !state.form.forms['booking'].bookDate)
    ? false
    : state.form.forms['booking'].bookDate.valid,
});

const mapDispatchToProps = dispatch => ({
  updateField: (value, valid) => dispatch(FormActions.updateField('booking', 'bookDate', value, valid)),
})

export default translate()(connect(mapStateToProps, mapDispatchToProps)(DateTimePickerInput));
