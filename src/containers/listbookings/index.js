import React, { Component } from "react";
import { Container, Content, Spinner } from "native-base";
import moment from "moment/min/moment-with-locales";
import { connect } from "react-redux";

import BookingActions from "../../redux/BookingRedux";
import ListItemBooking from "./components/item";
import ListItemEmptyBooking from "./components/emptyItem";

class ListBookings extends Component {
  static navigationOptions = ({ screenProps }) => {
    const { t } = screenProps;
    return {
      headerTitle: t("common:ListBookings"),
    }
  }

  constructor(props) {
    super(props);

    this.getBookings(props.userId);
  }

  getBookings = (userId) => {
    const from = moment().format();
    const to = moment().add(1, "year").format();
    this.props.getBookings(from, to, userId);
  }

  render() {
    const { bookings, isFetching } = this.props;
    return (
      <Container>
        <Content>
          {isFetching &&
            <Spinner />
          }
          {!isFetching && bookings.map((item, key) => (
            <ListItemBooking key={key} bookDate={item.bookDate} facility={item.facilityName}
              by={item.functionalUnitDesignation} isMidDay={item.isMidDay}
              isNight={item.isNight} strategy={item.strategy} id={item.id} />
          ))}
          {!isFetching && bookings.length === 0 &&
            <ListItemEmptyBooking/>
          }
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  bookings: state.booking.bookings,
  isFetching: state.booking.isFetchingBookings,
  failed: state.booking.failedListMesage,
  userId: state.profile.userId,
});

const mapDispatchToProps = dispatch => ({
  getBookings: (from, to, userId) => dispatch(BookingActions.getBookingListRequest(from, to, userId)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ListBookings);
