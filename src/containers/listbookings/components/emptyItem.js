import React from "react";
import {ListItem, Text} from "native-base";
import { translate } from "react-i18next";

import styles from "../../../components/Styles";

const ListItemEmptyBooking = ({ t }) => (
  <ListItem style={styles.center}>
    <Text >{t("booking:listEmpty")}</Text>
  </ListItem>
)

export default translate()(ListItemEmptyBooking);
