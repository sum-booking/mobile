import React, { Component } from "react";
import { View, TouchableOpacity, Alert } from "react-native";
import { Text, ListItem, Icon, Spinner, Toast } from "native-base";
import moment from "moment/min/moment-with-locales";
import { translate } from "react-i18next";
import { connect } from "react-redux";

import BookingActions from "../../../redux/BookingRedux";

import AgendaItem from "../../../components/AgendaItem";
import styles from "../../../components/Styles";
import { capitalizeFirstLetter } from "../../../helpers";

class ListItemBooking extends Component {
  handleDelete = () => {
    const { t, id, deleteBooking } = this.props;
    const title = t("booking:deleteTitle");
    const message = t("booking:deleteMessage");
    const cancel = t("common:cancel");
    const ok = t("common:ok");
            Alert.alert(title, message, [
                { text: cancel, style: "cancel" },
                {
                    text: ok, onPress: () => deleteBooking(id)
                }
            ]);
  }

  componentDidUpdate(prevProps) {
    const { t, deleteError, isDeleting } = this.props;
    if (prevProps.isDeleting !== isDeleting && !isDeleting) {
      Toast.show({
        text: deleteError ? t(`api:${deleteError}`) : t("booking:deleted"),
        type: deleteError ? "warning" : "success",
        buttonText: t("common:ok"),
        duration: 5000,
        position: "top"
      });
    }
  }

  render() {
    const { i18n, bookDate, facility, by, isMidDay, isNight, strategy, isDeleting, id } = this.props;
    const momentDate = moment(bookDate).locale(i18n.language);
    const shortMonth = capitalizeFirstLetter(momentDate.format("MMM"));
    const shortWeek = capitalizeFirstLetter(momentDate.format("ddd"));
    return (
      <ListItem style={{marginLeft: 0, paddingLeft: 0, paddingRight: 0, marginRight: 0, paddingTop: 0, marginTop: 0, paddingBottom: 0}}>
        <View style={{flex: 0.1}}>
          <Text style={[styles.dayText]}>{shortMonth}</Text>
          <Text style={[styles.dayNum]}>{momentDate.format("D")}</Text>
          <Text style={[styles.dayText]}>{shortWeek}</Text>
        </View>
        <View style={{ flex: 0.8 }}>
          <AgendaItem facility={facility} by={by} isMidDay={isMidDay} strategy={strategy} isNight={isNight} />
        </View>
        <View style={[{flex: 0.1}, styles.center]}>
          <TouchableOpacity onPress={this.handleDelete} disabled={isDeleting !== null} style={{padding: 10}}>
            {isDeleting === id
              ? <Spinner color="red" size="small" style={{height: 20}}/>
              : <Icon name="ios-trash-outline" style={{ color: (isDeleting !== null) ? "grey" : "red" }} />
            }
          </TouchableOpacity>
        </View>
      </ListItem>
    )
  }
}

const mapStateToProps = state => ({
  deleteError: state.booking.deleteFailedMessage,
  isDeleting: state.booking.isDeletingBooking,
});

const mapDispatchToProps = dispatch => ({
  deleteBooking: (id) => dispatch(BookingActions.deleteBookingRequest(id)),
})

export default translate()(connect(mapStateToProps, mapDispatchToProps)(ListItemBooking));
