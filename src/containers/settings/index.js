import React, { Component } from "react";
import { View, Text, Alert, ScrollView } from "react-native";
import { List, ListItem, Container } from "native-base";
import { translate } from "react-i18next";
import { Util } from "expo";
import { connect } from "react-redux";

import { persistor } from "../../redux/createStore";
import { Styles, Avatar, Field } from "../../components";

class Settings extends Component {
  static navigationOptions = ({ screenProps }) => {
    const { t } = screenProps
    return {
      title: t("common:Settings"),
      headerTruncatedBackTitle: "Volver"
    }
  }

  handleLogout = () => {
    const { t } = this.props;

    const title = t("settings:logout");
    const message = t("settings:logoutMessage");
    const cancel = t("common:cancel");
    const ok = t("common:ok");

    const logout = () => {
      persistor.purge().then(() => {
        Util.reload();
      });
    }

    Alert.alert(title, message, [
      { text: cancel, style: "cancel" },
      { text: ok, onPress: logout }
    ]);
  }

  render() {
    const { t, pictureUrl, firstName, lastName, email, phoneNumber,
      consortium, functionalUnit, navigation } = this.props;
    return <View style={{ flex: 1 }}>
      <Container navigation={navigation}>
        <ScrollView style={{ backgroundColor: "white" }}>
          <View style={[Styles.header, Styles.center, Styles.whiteBg]}>
            <Avatar size={175} picture={pictureUrl} />
          </View>
          <List>
            <ListItem itemDivider>
              <Text>{t("settings:profile").toUpperCase()}</Text>
            </ListItem>
            <Field label={t("settings:name")} value={`${firstName} ${lastName}`} disabled />
            <Field label={t("settings:email")} value={email} disabled />
            <Field label={t("settings:phoneNumber")} value={phoneNumber} disabled last />
            <ListItem itemDivider>
              <Text>{t("settings:functionalUnit").toUpperCase()}</Text>
            </ListItem>
            <Field label={t("settings:consortium")} value={consortium} disabled />
            <Field label={t("settings:functionalUnit")} value={functionalUnit} disabled last />
          </List>
        </ScrollView>
      </Container>
    </View>;
  }
}

const mapStateToProps = state => ({
  pictureUrl: state.profile.pictureUrl,
  firstName: state.profile.firstName,
  lastName: state.profile.lastName,
  email: state.profile.email,
  phoneNumber: state.profile.phoneNumber,
  consortium: state.profile.consortium,
  functionalUnit: state.profile.functionalUnit,
  phoneNotifications: state.profile.phoneNotifications,
  emailNotifications: state.profile.emailNotifications,
});

export default translate()(connect(mapStateToProps)(Settings));
