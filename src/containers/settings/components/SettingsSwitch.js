import React, { Component } from "react";
import { Switch } from "native-base";

import variables from "../../../../native-base-theme/variables/commonColor";

export default class SettingsSwitch extends Component {
    render() {
        return <Switch
            value={this.props.value}
            onValueChange={this.props.toggle}
            onTintColor="rgba(80, 210, 194, .5)"
            thumbTintColor={this.props.value ? variables.brandInfo : "#BEBEC1"}
        />;
    }
}
