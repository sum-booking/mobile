import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Container, Icon } from "native-base";
import { translate } from "react-i18next";
import { Agenda, LocaleConfig } from "react-native-calendars";
import moment from "moment/min/moment-with-locales";
import { connect } from "react-redux";
import { groupBy } from "lodash";

import BookingActions from "../../redux/BookingRedux";

import AgendaItem from "../../components/AgendaItem";
import { capitalizeFirstLetterArray, capitalizeFirstLetter } from "../../helpers";
import styles from "../../components/Styles";

import variables from "../../../native-base-theme/variables/commonColor";

class Home extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    const { t } = screenProps;
    const { params = {} } = navigation.state;

    const right = <TouchableOpacity style={{ padding: 5 }}
      onPress={params.handleAdd ? () => params.handleAdd() : () => null}>
      <Icon name="ios-add-outline" style={{ color: variables.gray, fontSize: 50 }} />
    </TouchableOpacity>

    return {
      title: t("common:Home"),
      headerRight: right,
    }
  }

  constructor(props) {
    super(props);

    const currentDay = moment().format("YYYY-MM-DD");
    this.state = {
      items: {},
      currentDay: currentDay,
      currentLoadedDay: currentDay,
    };

    LocaleConfig.locales[props.i18n.language] = {
      monthNames: capitalizeFirstLetterArray(moment.localeData(props.i18n.language).months()),
      monthNamesShort: capitalizeFirstLetterArray(moment.localeData(props.i18n.language).monthsShort()),
      dayNames: capitalizeFirstLetterArray(moment.localeData(props.i18n.language).weekdays()),
      dayNamesShort: capitalizeFirstLetterArray(moment.localeData(props.i18n.language).weekdaysShort())
    }

    LocaleConfig.defaultLocale = props.i18n.language;

    props.navigation.setParams({ handleAdd: this.handleAdd, avatar: this.props.pictureUrl });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.fetching !== this.props.fetching && !this.props.fetching) {
      const gruppedBookings = this.formatBookings(this.props.bookings);
      this.setState({ items: gruppedBookings });
    }
  }

  formatBookings = (bookings) => {
    const gruppedBookings = this.groupBookings(bookings);
    const month = moment(this.state.currentLoadedDay, "YYYY-MM-DD");
    const secondMonth = moment(this.state.currentLoadedDay, "YYYY-MM-DD").add(1, "months");
    this.fixEmptyDay(gruppedBookings, month);
    this.fixEmptyDay(gruppedBookings, secondMonth);
    return gruppedBookings;
  }

  groupBookings = (bookings) => {
    if (bookings.length > 0) {
      const grouped = groupBy(bookings, item => {
        const grp = new moment(item.bookDate);
        return grp.format("YYYY-MM-DD");
      });
      return grouped;
    } else {
      return {};
    }
  }

  fixEmptyDay = (bookings, month) => {
    const daysInMonth = month.daysInMonth();
    for (var i = 1; i <= daysInMonth; i++) {
      const dateString = month.date(i).format("YYYY-MM-DD");
      if (!bookings.hasOwnProperty(dateString)) {
        bookings[dateString] = [];
      }
    }
  }

  handleAdd = (day) => {
    let date = this.state.currentDay;
    if (day) {
      date = day;
    }
    this.props.navigation.navigate("NewBooking", { date, onNavigateBack: this.handleOnNavigateBack });
  }

  loadItems = (day, force) => {
    const { dateString } = day;
    if (!this.state.items.hasOwnProperty(dateString) || force) {
      const from = new moment(dateString).format();
      const to = new moment(dateString).add(1, "months").endOf("month").format();
      this.props.getBookings(from, to);
      this.setState({ currentLoadedDay: dateString });
    }
  }

  renderItem = (item, firstItemInDay) => {
    return (
      <AgendaItem isMidDay={item.isMidDay} isNight={item.isNight}
        strategy={item.strategy} facility={item.facilityName} by={item.functionalUnitDesignation}
        firstItemInDay={firstItemInDay} />
    );
  }

  rowHasChanged = (r1, r2) => {
    return r1.id !== r2.id;
  }

  renderDay = (date) => {
    if (date) {
      const { i18n } = this.props;
      const today = moment().format("YYYY-MM-DD");
      const momentDate = moment(date.dateString).locale(i18n.language);
      const addTodayStyle = today === date.dateString ? styles.today : undefined;
      const shortMonth = capitalizeFirstLetter(momentDate.format("MMM"));
      const shortWeek = capitalizeFirstLetter(momentDate.format("ddd"));
      return (
        <View style={[styles.day, styles.topBorder]}>
          <Text style={[styles.dayText, addTodayStyle]}>{shortMonth}</Text>
          <Text style={[styles.dayNum, addTodayStyle]}>{momentDate.format("D")}</Text>
          <Text style={[styles.dayText, addTodayStyle]}>{shortWeek}</Text>
        </View>
      )
    }
    return <View style={styles.day} />
  }

  renderEmptyDate = (day) => {
    const { t } = this.props;
    const tmpDate = day.toISOString().slice(0, -1);
    return (
      <View style={[styles.item, styles.topBorder]} >
        <TouchableOpacity style={styles.emptyDate} onPress={() => this.handleAdd(tmpDate)}>
          <Text style={{color: variables.brandInfo}}>{t("booking:addTitle")}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  handleDayPress = (day) => {
    this.setState({ currentDay: day.dateString });
    this.loadItems(day);
  }

  handleOnNavigateBack = () => {
    this.loadItems({ dateString: this.state.currentDay }, true);
  }

  onDayChange = (day) => {
    this.setState({ currentDay: day.dateString });
    this.loadItems(day);
  }

  render() {
    return <Container>
      <Agenda
        items={this.state.items}
        loadItemsForMonth={this.loadItems}
        onDayPress={this.handleDayPress}
        onDayChange={this.onDayChange}
        selected={moment().format("YYYY-MM-DD")}
        renderItem={this.renderItem}
        renderEmptyDate={this.renderEmptyDate}
        rowHasChanged={this.rowHasChanged}
        renderDay={this.renderDay}
        pastScrollRange={1}
        futureScrollRange={12}
      />
    </Container >;
  }
}

const mapStateToProps = state => ({
  pictureUrl: state.profile.pictureUrl,
  bookings: state.booking.bookings,
  fetching: state.booking.isFetchingBookings
});

const mapDispatchToProps = dispatch => ({
  getBookings: (from, to) => dispatch(BookingActions.getBookingListRequest(from, to)),
})

export default translate()(connect(mapStateToProps, mapDispatchToProps)(Home));
