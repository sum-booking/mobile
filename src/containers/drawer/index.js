import React, { Component } from "react";
import { View, StyleSheet, ImageBackground } from "react-native";
import { Button, Container, H2, Icon } from "native-base";
import { translate } from "react-i18next";
import { Util } from "expo";
import moment from "moment/min/moment-with-locales";

import { persistor } from "../../redux/createStore";
import { WindowDimensions } from "../../components";

import variables from "../../../native-base-theme/variables/commonColor";
const drawerBg = require("../../../assets/images/drawer.jpg");

class Drawer extends Component {
  navigateHome = () => {
    this.props.navigation.navigate("Home");
  }

  navigateNewBooking = () => {
    const currentDay = moment().format("YYYY-MM-DD");
    this.props.navigation.navigate("NewBooking", { currentDay });
  }

  navigateSettings = () => {
    this.props.navigation.navigate("Settings");
  }

  navigateListBookings = () => {
    this.props.navigation.navigate("ListBookings");
  }

  logout = () => {
    persistor.purge();
    Util.reload();
  }

  isActive = (key) => {
    const navState = this.props.navigation.state;
    const currentIndex = navState.index;
    return navState.routes[currentIndex].key === key;
  }

  render() {
    const { t } = this.props;

    return <ImageBackground source={drawerBg} style={style.img}>
      <Container style={style.container}>
        <View style={style.drawerItemsContainer}>
          <View style={style.drawerItems}>
            <DrawerItem icon="ios-home-outline"
              onPress={this.navigateHome} label={t("common:Home")} active={this.isActive('Home')} />
            <DrawerItem icon={"ios-add-outline"}
              onPress={this.navigateNewBooking} label={t("common:NewBooking")}
              active={this.isActive('NewBooking')} />
            <DrawerItem icon={"ios-list-outline"}
              onPress={this.navigateListBookings} label={t("common:ListBookings")}
              active={this.isActive('ListBookings')} />
          </View>
        </View>
        <View style={style.footerRow}>
          <DrawerItem onPress={this.navigateSettings} icon="ios-person-outline"
            label={t("common:Settings")} active={this.isActive('Settings')} />
            {__DEV__ &&
              <DrawerItem onPress={this.logout} icon="ios-person-outline"
              label="logout" active={this.isActive('Settings')} />
            }
        </View>
      </Container>
    </ImageBackground >
  }
}

class DrawerItem extends Component {
  render() {
    const { label, onPress, active, icon } = this.props;
    return <Button onPress={onPress} full transparent style={style.drawerItemBtn}>
      <Icon name={icon} style={{ color: "rgba(255, 255, 255, .5)", paddingRight: variables.contentPadding }} />
      <H2 style={{ color: active ? "white" : "rgba(255, 255, 255, .5)" }}>{label}</H2>
    </Button>;
  }
}

const style = StyleSheet.create({
  img: {
    ...StyleSheet.absoluteFillObject,
    ...WindowDimensions
  },
  container: {
    backgroundColor: "rgba(10, 63, 0, .9)",
    paddingHorizontal: variables.contentPadding,
  },
  footerRow: {
    flex: 0.2,
    justifyContent: "flex-start",
    alignItems: "stretch"
  },
  drawerItemsContainer: {
    flex: 0.8,
    justifyContent: "flex-start",
    alignItems: "stretch",
    paddingVertical: variables.contentPadding * 2.5,
    borderBottomColor: "grey",
    borderBottomWidth: 1
  },
  drawerItemBtn: {
    justifyContent: "flex-start",
    alignItems: "stretch"
  }
});

export default translate()(Drawer);
