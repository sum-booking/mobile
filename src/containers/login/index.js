import React from "react";
import { View, ImageBackground, StyleSheet, Alert, Linking } from "react-native";
import { Container, Button, Text } from "native-base";
import { translate } from "react-i18next";
import { connect } from "react-redux";
import { AuthSession } from "expo";

import Mark from "./components/Mark";

import { Styles } from "../../components";
import AuthActions from "../../redux/AuthRedux";
import { toQueryString, getConfigKey, guid } from "../../helpers";

import variables from "../../../native-base-theme/variables/commonColor";
const loginBg = require("../../../assets/images/login.jpg");

class Login extends React.Component {
  static navigationOptions = {
    header: null
  }

  signIn = async () => {
    const { t } = this.props;
    const auth0ClientId = getConfigKey("auth0ClientId");
    const auth0Domain = getConfigKey("auth0Domain");
    const auth0Audience = getConfigKey("auth0ApiAudience");
    const redirectUrl = AuthSession.getRedirectUrl();
    const result = await AuthSession.startAsync({
      authUrl: `https://${auth0Domain}/authorize` + toQueryString({
        client_id: auth0ClientId,
        response_type: 'code',
        scope: 'openid profile email offline_access',
        redirect_uri: redirectUrl,
        lockType: "email",
        state: guid(''),
        audience: auth0Audience
      }),
    });
    if (result.type === "success") {
      this.props.login({ ...result.params, redirect_uri: redirectUrl });
    } else {
      Alert.alert(t("common:error"), t("login:cancelledMessage"))
    }
  }

  componentDidUpdate(prevProps) {
    const { authErrorCode, t, profileErrorCode } = this.props;

    if (prevProps.authErrorCode !== authErrorCode && authErrorCode) {
      if (authErrorCode === "unauthorized") {
        Alert.alert(t("common:error"), t("login:unauthorizedErrorMsg"))
      } else {
        Alert.alert(t("common:error"), t("login:unknownErrorMsg"))
      }
    }

    if (prevProps.profileErrorCode !== profileErrorCode && profileErrorCode) {
      Alert.alert(t("common:error"), t("login:unknownProfileErrorMsg"))
    }
  }

  render() {
    const { t, isFetching } = this.props;
    return <ImageBackground source={loginBg} style={style.img}>
      <Container style={Styles.imgMask}>
        <View style={style.content}>
          <View style={style.logo}>
            <Mark loading={isFetching} />
          </View>
          <View style={style.loginBtn}>
            <Button primary block onPress={this.signIn}>
              <Text>{t("login:signIn")}</Text>
            </Button>
          </View>
        </View>
      </Container>
    </ImageBackground>;
  }
}

const style = StyleSheet.create({
  img: {
    flex: 1
  },
  content: {
    flex: 1,
    justifyContent: "flex-end"
  },
  logo: {
    flex: 0.6,
    alignItems: "center",
    justifyContent: "center"
  },
  loginBtn: {
    flex: 0.2,
    justifyContent: "flex-start",
    padding: 25
  },
  title: {
    marginTop: variables.contentPadding * 2,
    color: "white",
    textAlign: "center"
  }
});

const mapStateToProps = state => ({
  isFetching: state.auth.isFetching || state.profile.isFetching,
  authErrorCode: state.auth.errorCode,
  profileErrorCode: state.profile.errorCode
});

const mapDispatchToProps = dispatch => ({
  login: (request) => dispatch(AuthActions.loginRequest(request))
});

export default translate()(connect(mapStateToProps, mapDispatchToProps)(Login));
