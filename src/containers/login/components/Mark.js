import React from "react";
import { View, Image } from "react-native";
import { Spinner } from "native-base";

import commonColor from "../../../../native-base-theme/variables/commonColor";
const logo = require("../../../../assets/images/logo.png")

const getStyle = (size) => {
    return {
        width: size,
        height: size,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: size / 2
    }
}

export default class Mark extends React.Component {

    render() {
        return <View style={[getStyle(230), { borderColor: "rgba(255, 255, 255, .3)", borderWidth: 2 }]}>
            <View style={[getStyle(210), { borderColor: "rgba(255, 255, 255, .5)", borderWidth: 2 }]}>
                <View style={[getStyle(180), { borderColor: "white", borderWidth: 2 }]}>
                    <View style={[getStyle(150), { backgroundColor: commonColor.white }]}>
                        {this.props.loading && <Spinner color="grey" />}
                        {!this.props.loading && <Image source={logo} style={{ height: 150, width: 150}} />}
                    </View>
                </View>
            </View>
        </View>;
    }
}
