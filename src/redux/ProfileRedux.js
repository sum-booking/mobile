import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  profileRequest: ['consortiumCode', 'functionalUnitCode', 'userId'],
  profileResponse: ['profile'],
  profileError: ['code'],
});

export const ProfileTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
  userId: null,
  firstName: null,
  lastName: null,
  pictureUrl: null,
  email: null,
  phoneNumber: null,
  consortium: null,
  consortiumCode: null,
  functionalUnit: null,
  functionalUnitCode: null,
  phoneNotifications: false,
  emailNotifications: false,
  isFetching: false,
  errorCode: null,
});

const profileRequest = state => Immutable.merge(state, { isFetching: true, errorCode: null });
const profileResponse = (state, { profile }) => Immutable.merge(state, [{
  isFetching: false, errorCode: null
}, profile]);
const profileError = (state, { code }) => Immutable.merge(state, { isFetching: false, errorCode: code });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PROFILE_REQUEST]: profileRequest,
  [Types.PROFILE_RESPONSE]: profileResponse,
  [Types.PROFILE_ERROR]: profileError,
});
