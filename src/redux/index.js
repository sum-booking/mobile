import { combineReducers } from 'redux';

export default combineReducers({
    auth: require('./AuthRedux').reducer,
    profile: require('./ProfileRedux').reducer,
    booking: require('./BookingRedux').reducer,
    facility: require('./FacilityRedux').reducer,
    form: require('./FormRedux').reducer,
});
