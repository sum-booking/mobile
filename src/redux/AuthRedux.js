import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  loginRequest: ['request'],
  loginResponse: ['accessToken', 'refreshToken', 'expiresAt'],
  loginError: ['code'],
  updateToken: ['accessToken', 'expiresAt'],
  logout: null,
});

export const AuthTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
  isFetching: false,
  errorCode: null,
  accessToken: null,
  refreshToken: null,
  expiresAt: null,
});

const loginRequest = (state) => Immutable.merge(state, { isFetching: true, errorCode: null });
const loginResponse = (state, { accessToken, refreshToken, expiresAt }) => Immutable.merge(state, { accessToken, refreshToken, expiresAt, isFetching: false, errorCode: null });
const loginError = (state, { code }) => Immutable.merge(state, { isFetching: false, errorCode: code });
const updateToken = (state, { accessToken, expiresAt }) => Immutable.merge(state, { accessToken, expiresAt });
const logout = () => INITIAL_STATE;

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: loginRequest,
  [Types.LOGIN_RESPONSE]: loginResponse,
  [Types.LOGIN_ERROR]: loginError,
  [Types.LOGOUT]: logout,
  [Types.UPDATE_TOKEN]: updateToken,
});
