import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  getFacilityListRequest: null,
  getFacilityListResponse: ['facilities'],
  getFacilityListFailed: ['message']
});

export const FacilityTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  facilities: [],
  isFetchingFacilities: false,
  failedListMesage: null,
});

const getListRequest = (state) => Immutable.merge(state, { isFetchingFacilities: true, failedListMesage: null });
const getListResponse = (state, { facilities }) => Immutable.merge(state, { facilities, isFetchingFacilities: false, failedListMesage: null });
const getListFailed = (state, { message }) => Immutable.merge(state, { failedListMesage: message, isFetchingFacilities: false });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_FACILITY_LIST_REQUEST]: getListRequest,
  [Types.GET_FACILITY_LIST_RESPONSE]: getListResponse,
  [Types.GET_FACILITY_LIST_FAILED]: getListFailed,
});
