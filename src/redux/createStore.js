import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import { persistStore, persistReducer } from "redux-persist";
import createSecureStore from "./secureStore";

import reducers from "./";
import sagas from "../sagas";

const storage = createSecureStore();
const persistConfig = {
  key: "root",
  storage,
  whitelist: ["profile", "auth"]
}

const persistedReducer = persistReducer(persistConfig, reducers)

const sagaMiddleware = createSagaMiddleware();
const enhancers = [applyMiddleware(sagaMiddleware)];

export const store = createStore(persistedReducer, compose(...enhancers))
export const persistor = persistStore(store);

sagaMiddleware.run(sagas);
