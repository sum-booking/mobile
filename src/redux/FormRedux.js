import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  initForm: ['name'],
  updateField: ['form', 'field', 'value', 'valid'],
  removeField: ['form', 'field'],
});

export const FormTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  forms: {}
});

const initForm = (state, {name}) => Immutable.merge(state, {forms: {[name]: []} });
const updateField = (state, {form, field, value, valid}) => {
  var formToMerge = Immutable.merge(state.forms[form], {[field]: {value, valid}});
  var formsToMerge = Immutable.merge(state.forms, {[form]: formToMerge});
  return Immutable.merge(state, {forms:formsToMerge});
}
const removeField = (state, {form, field}) => {
  var formToMerge = Immutable.without(state.forms[form], field);
  var formsToMerge = Immutable.merge(state.forms, {[form]: formToMerge});
  return Immutable.merge(state, {forms:formsToMerge});
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.INIT_FORM]: initForm,
  [Types.UPDATE_FIELD]: updateField,
  [Types.REMOVE_FIELD]: removeField,
});
