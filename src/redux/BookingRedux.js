import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  getBookingListRequest: ['from', 'to', 'userId'],
  getBookingListResponse: ['bookings'],
  getBookingListFailed: ['message'],
  addBookingRequest: ['form'],
  addBookingFailed: ['message'],
  addBookingResponse: null,
  deleteBookingRequest: ['id'],
  deleteBookingResponse: null,
  deleteBookingFailed: ['message'],
});

export const BookingTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  bookings: [],
  isFetchingBookings: false,
  failedListMesage: null,
  isAdding: false,
  addFailedMessage: null,
  deleteFailedMessage: null,
  isDeletingBooking: null,
});

const getListRequest = (state) => Immutable.merge(state, { isFetchingBookings: true, failedListMesage: null });
const getListResponse = (state, { bookings }) => Immutable.merge(state, { bookings, isFetchingBookings: false, failedListMesage: null });
const getListFailed = (state, { message }) => Immutable.merge(state, { failedListMesage: message, isFetchingBookings: false });
const addRequest = (state) => Immutable.merge(state, { isAdding: true, addFailedMessage: null });
const addFailed = (state, { message }) => Immutable.merge(state, { addFailedMessage: message, isAdding: false });
const addResponse = (state) => Immutable.merge(state, { addFailedMessage: null, isAdding: false });
const deleteRequest = (state, { id }) => Immutable.merge(state, { isDeletingBooking: id, deleteFailedMessage: null });
const deleteResponse = (state) => Immutable.merge(state, { isDeletingBooking: null, deleteFailedMessage: null });
const deleteFailed = (state, { message }) => Immutable.merge(state, { isDeletingBooking: null, deleteFailedMessage: message });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_BOOKING_LIST_REQUEST]: getListRequest,
  [Types.GET_BOOKING_LIST_RESPONSE]: getListResponse,
  [Types.GET_BOOKING_LIST_FAILED]: getListFailed,
  [Types.ADD_BOOKING_REQUEST]: addRequest,
  [Types.ADD_BOOKING_FAILED]: addFailed,
  [Types.ADD_BOOKING_RESPONSE]: addResponse,
  [Types.DELETE_BOOKING_REQUEST]: deleteRequest,
  [Types.DELETE_BOOKING_RESPONSE]: deleteResponse,
  [Types.DELETE_BOOKING_FAILED]: deleteFailed,
});
